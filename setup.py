from setuptools import setup, find_packages

setup(
    name='hydrodatamx',
    version='0.1',
    url='https://gitlab.com/Zaul_AE/HydroDataMx.git',
    license='GNU 3.0',
    author='Saul Arciniega Esparza',
    author_email='zaul_ae@hotmail.com',
    description='Set of tools for download, manage and analyze hydrological and climate databases from Mexico',
    packages=find_packages(exclude=['build', 'dist', 'hydrodatamx.egg-info']),
    classifiers=[
            'Development Status :: HydroToolsMx',
            'Intended Audience :: Hydrology, Climate',
            'Programming Language :: Python',
            'Topic :: Software Development :: Libraries :: Python Modules',
            'Programming Language :: Python :: 3.7 or newer'
        ],
    include_package_data=True,
    install_requires=[
         'numpy',
         'pandas',
    ],
)
