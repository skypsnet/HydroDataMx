# -*- coding: utf-8 -*-
"""
Set of tools to process timeseries


Author:
Saul Arciniega Esparza | zaul_ae@hotmail.com
Hydrogeology Group, Faculty of Engineering
National Autonomous University of Mexico (UNAM)
Mexico City
"""

import numpy as np
import pandas as pd


# ==============================================================================
# Tools
# ==============================================================================

def days_without_data(data, by='month'):
    """
    Returns a DataFrame with the number of nodata values by month or year

    Parameters
    ----------
    data : DataFrame
        Input data time series
    by : string, optional
        Time aggregation ('month', 'year'). The default is 'month'.

    Returns
    -------
    no_data : DataFrame
        Returns the number of days with no values.
    """

    if type(data) is str:
        data = pd.read_csv(data, parse_dates=[0], index_col=[0])

    no_data = data.isna()
    if by.lower() == 'month':
        no_data = no_data.groupby(no_data.index - pd.offsets.MonthBegin()).sum()
    elif by.lower() == 'year':
        no_data = no_data.groupby(no_data.index.year).sum()
    return no_data


def filter_stations(data, year1=None, year2=None, min_years=None, max_nodata=None):
    """
    Returns a DataFrame with the gauges that meet some constraints

    Parameters
    ----------
    data : str, DataFrame
        Input timeserie of climatological gauges
    year1 : int, optional
        Returns gauges which initial year of records ir lower or equal than year1.
        The default is None.
    year2 : int, optional
        Returns gauges which final year of records ir greater or equal than year2.
        The default is None.
    min_years : int, optional
        Returns gauges which number of years of records are larger or equal than
        min years. By default this is ignored
    max_nodata : float, int, optional
        Returns the gauges which percent of no data is lower or equal than max_nodata.
        By default it is ignored. max_nodata must be between 0 and 100

    Returns
    -------
    DataFrame
        Filtered timeseries

    """

    if type(data) is str:
        data = pd.read_csv(data, parse_dates=[0], index_col=[0])

    selected_codes = []
    year_list1, year_list2 = [], []
    for code in data.columns:
        records = data[code]
        mask = records.isna()
        if (~mask).sum() == 0:
            continue
        records1 = records[~mask]
        date1 = records1.index.min()
        date2 = records1.index.max()
        yearr1 = date1.year
        yearr2 = date2.year
        year_low, year_max = year1, year2
        if year_low is None:
            year_low = yearr1
        if year_max is None:
            year_max = yearr2
        if yearr1 > year_low or yearr2 < year_max:
            continue
        num_years = yearr2 - yearr1 + 1
        if min_years is not None:
            if num_years < min_years:
                continue
        mask = records.loc[date1:date2].isna()
        len_nodata = mask.sum() / len(records1) * 100
        if max_nodata is not None:
            if len_nodata > max_nodata:
                continue
        selected_codes.append(code)
        year_list1.append(yearr1)
        year_list2.append(yearr2)

    if len(selected_codes) == 0:
        return []

    start = np.min(year_list1)
    end = np.max(year_list2)

    dataset = data.loc[f'{start}-01-01':f'{end}-12-23', selected_codes]
    return dataset


def monthly_aggregation(data, stat="mean", nodata_days=30):
    """
    Returns a DataFrame with the monthly aggregation of daily data
    ignoring months with several null records

    Parameters
    ----------
    data : DataFrame, string
        Input daily time series as DataFrame of as a csv file name
    stat : string, optional
        Statistic for the monthly aggregation. The default is "mean".
    nodata_days : TYPE, optional
        Number of days with null records to ignore a month. The default is 30.

    Returns
    -------
    monthly : DataFrame
        monthly aggregation.

    """

    if type(data) is str:
        data_copy = pd.read_csv(data, parse_dates=[0], index_col=[0])
    else:
        data_copy = data.copy()

    grouped = data_copy.resample('1M').count()
    monthly = data_copy.resample('1M').agg(stat)
    monthly.values[grouped.values < (30 - nodata_days)] = np.nan
    monthly.index = monthly.index - pd.offsets.MonthBegin()

    return monthly


def annual_aggregation(data_month, stat="mean", nodata_months=0):
    """
    Returns a DataFrame with the annual aggregation of monthly data
    ignoring years with several null records

    Parameters
    ----------
    data_month : DataFrame, string
        Input monthly time series as DataFrame of as a csv file name
    stat : string, optional
        Statistic for the annual aggregation. The default is "mean".
    nodata_months : TYPE, optional
        Number of months with null records to ignore a year. The default is 30.

    Returns
    -------
    annual : DataFrame
        annual aggregation.

    """

    if type(data_month) is str:
        data_copy = pd.read_csv(data_month, parse_dates=[0], index_col=[0])
    else:
        data_copy = data_month.copy()

    grouped = data_copy.groupby(data_copy.index.year).count()
    annual = data_copy.groupby(data_copy.index.year).agg(stat)
    annual.values[grouped.values < (12 - nodata_months)] = np.nan

    return annual

