# -*- coding: utf-8 -*-
"""
HydroDataMx set of tools to download ana analyze hydrological
and climatological databases from Mexico


Author:
Saul Arciniega Esparza | zaul_ae@hotmail.com
Hydrogeology Group, Faculty of Engineering
National Autonomous University of Mexico (UNAM)
Mexico City
"""

__version__ = "0.1"


from .smn import SMNdatabase
from . import tools

