# **HydroDataMx**

**hydrodatamx** is a set of tools for download, manage and analyze hydrological and climate databases from Mexico.

## Features

* **smn**: scripts for download daily climatological dataset from the [Servicio Meteorologico Nacional](https://smn.conagua.gob.mx/es/).
* **tools**: scripts to post-process and analyze datasets. 


## How to install

You can use **git** as follow:

```
git clone https://gitlab.com/Zaul_AE/HydroDataMx.git
cd HydroDataMx
python setup.py install
```

Or you can download the [repository](https://gitlab.com/Zaul_AE/HydroDataMx) and install it from console:

```
python setup.py install
```


## Basic Example

First import the module:

```python
import matplotlib.pyplot as plt
import hydrodatamx as hd
```

Now initialize an SMN database and query data:

```python
# initialize database
smn = hd.SMNdatabase()

# get list of stations for Aguascalientes state
smn_list = smn.search_stations(state="Aguascalientes")

# read station with id="2003"
info, data = smn.get_station_data("2003")

# plot precipitation data
data.Prec.plot()
plt.show()

# get number of days without data (NaNs)
nodata = hd.tools.days_without_data(data)
print(nodata)
```

**New feature**. In a Jupyter Notebook now you can use am interactive map to select multiple stations:

1. Initialize the interactive map:

```python
# initialize database
smn = hd.SMNdatabase()
# Initialize interactive map
smn.map()
```

2. Select your stations.
3. Donwload the data:

```python
info, data = info, data = smn.get_selected_stations_data()
# plot the first column
data.iloc[:, 0].plot()
```

More features coming soon.

## Example Notebooks

*Coming soon*

## Author

Ph.D. Saúl Arciniega Esparza, Full Time Associate Professor | [Hydrogeology Group](https://www.ingenieria.unam.mx/hydrogeology/), [Faculty of Engineering](https://www.ingenieria.unam.mx/) at the [National Autonomous University of Mexico](https://www.unam.mx/).

[LinkedIn](https://www.linkedin.com/in/saularciniegaesparza/) | [Twitter](https://twitter.com/zaul_arciniega) | [ResearchGate](https://www.researchgate.net/profile/Saul-Arciniega-Esparza)

<img src="/img/logo_01.png" alt="" width="200"/>
